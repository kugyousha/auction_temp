class Item < ActiveRecord::Base
  belongs_to :user
  validates :name, presence: true, uniqueness: true
  validates :user_id, presence: true
  validates :reserve, presence: true
 
  def start_auction(current_user)
    if current_user.id == self.user_id && !started
      self.started = true
      self.save
    end
  end
  
  def place_bid(current_user, new_bid)
    if self.started && !self.complete && new_bid > self.highest_bid
      self.highest_bid = new_bid
      self.highest_bidder_id = current_user.id
    end
  end
  
  def close_auction(current_user)
    if self.started && self.user_id == current_user.id
      if self.highest_bid > self.reserve
        self.sold = true
      end
      self.complete = true
    end
  end
  
  def status
    status = "" #status helper method
    if self.complete && self.highest_bid > reserve
      status = "success"
    elsif self.complete
      status = "failure"
    elsif self.started && !self.complete
      status = "started"
    else
      status = "not started"
    end
  end
  def price
    price = 0
    self.highest_bid > self.reserve ?  price = self.highest_bid : price = self.reserve
  end
  
  def reserve_met
    reserve_met = self.highest_bid > self.reserve
    reserve_met ? 'yes' : 'no'
  end
  def current_bidder
    self.highest_bidder_id ? User.find_by_id(highest_bidder_id).name : "no bids yet"
  end
  def current_status
    "Item name: #{self.name}, Auctioner: #{User.find_by_id(self.user_id).name}, Status: #{status}, Price: #{price}, Reserve met: #{reserve_met}, Highest Bidder: #{current_bidder}"
  end
end
