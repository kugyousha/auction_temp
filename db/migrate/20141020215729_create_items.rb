class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
      t.string   :name
      t.integer  :user_id
      t.boolean  :started, default: false
      t.boolean  :complete, default: false
      t.float    :reserve
      t.float    :highest_bid, default: 0.0
      t.integer  :highest_bidder_id
      t.boolean  :sold, default: false

      t.timestamps
    end
  end
end
