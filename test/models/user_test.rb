require 'test_helper'

class UserTest < ActiveSupport::TestCase
  
  def setup
    @user = User.create({name: "user 1"})
  end
  
  def test_user_name
    assert_equal @user.name, "user 1"
  end
  
  def test_respond
    assert_respond_to @user, 'name'
  end
  
end
