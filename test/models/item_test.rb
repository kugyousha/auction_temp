require 'test_helper'

class ItemTest < ActiveSupport::TestCase
  def setup
    @user = User.create({name: "user 1"})
    @item = @user.items.new({name: "lamp", reserve: 10.50})
    @item.save
    @another_user = User.create(name: "user 2")
    
  end
  
  def test_user_name
    assert_equal @item.name, "lamp"
    assert_equal true, @item.valid?
  end
  
  def test_respond
    assert_respond_to @item, 'name'
    assert_respond_to @item, 'user_id'
    assert_respond_to @item, 'reserve'
    assert_respond_to @item, 'started'
    assert_respond_to @item, 'highest_bidder_id'
    assert_respond_to @item, 'current_status'
  end
  
  def test_start_an_auction
    @item.start_auction(@user)
    assert_equal @item.started, true
  end
  
  def test_another_user_cant_start_auction
    
    @item.start_auction(@another_user)
    assert_equal @item.started, false
  end
  
  def test_bid_on_an_item
    @item.start_auction(@user)
    @item.place_bid(@another_user, 30.00)
    assert_equal @item.highest_bid, 30.00
    assert_equal @item.highest_bidder_id, @another_user.id
    def test_bid_smaller_than_highest_bid
      @item.place_bid(@another_user, 3.00)
      assert_equal @item.highest_bid, 30.00
    end
  end
  
  def test_close_auction
    @item.start_auction(@user)
    @item.close_auction(@user)
    assert_equal @item.complete, true
  end
  
  def test_close_unstarted_auction
    @item.close_auction(@user)
    assert_equal @item.complete, false
  end
  
  def test_close_another_user_auction
    @item.start_auction(@user)
    @item.close_auction(@another_user)
    assert_equal @item.complete, false
  end
  
  def test_current_status_of_auction_success
    assert_match /not started/, @item.current_status
    @item.start_auction(@user)
    assert_match /Status: started/, @item.current_status
    @item.place_bid(@another_user, 30.00)
    assert_match /Highest Bidder: #{@another_user.name}/, @item.current_status
    assert_match /Reserve met: yes/, @item.current_status
    @item.close_auction(@user)
    assert_match /Status: success/, @item.current_status
  end
  
  def test_current_status_of_auction_failure
    assert_match /not started/, @item.current_status
    @item.start_auction(@user)
    assert_match /Status: started/, @item.current_status
    @item.place_bid(@another_user, 3.00)
    assert_match /Highest Bidder: #{@another_user.name}/, @item.current_status
    assert_match /Reserve met: no/, @item.current_status
    @item.close_auction(@user)
    assert_match /Status: failure/, @item.current_status
  end
  
end
